import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class App {
	public static void main(String[] args) {
		//Exemplos de List
		List<Integer> numeros = new ArrayList<>();
		
		numeros.add(10);
		numeros.add(50);
		numeros.add(40);
		numeros.add(40);
		
		System.out.println("Tamanho da lista: " + numeros.size());
		
		for(int numero : numeros) {
			System.out.println(numero);
		}
		
		numeros.remove(1);
		
		System.out.println("Tamanho da lista: " + numeros.size());
		System.out.println(numeros.indexOf(40));
		
		//Exemplos de Set
		Set<String> palavras = new HashSet<>();
//		Set<String> palavras = new TreeSet<>();//mantém ordenado
		
		palavras.add("Batata");
		palavras.add("Feijão");
		palavras.add("Amendoim");
		palavras.add("Cebola");
		palavras.add("Cebola");
		palavras.add("Cebola");
		
		for(String palavra : palavras) {
			System.out.println(palavra);
		}
		
		System.out.println(palavras.contains("Beterraba"));
		
		//Exemplos de Map
//		Map<String, String> mcCardapio = new HashMap<>();
		Map<String, String> mcCardapio = new TreeMap<>();//Mantém as chaves ordenadas
		
		mcCardapio.put("Seg", "McNífico com Bacon");
		mcCardapio.put("Ter", "Triplo Cheeseburguer");
		mcCardapio.put("Qua", "Big Mac");
		mcCardapio.put("Qui", "Cheddar McMelt");
		mcCardapio.put("Sex", "Quarteirão com Queijo");
		
		System.out.println(mcCardapio.get("Qui"));
		System.out.println(mcCardapio.get("Sab"));
		
		for(String chave : mcCardapio.keySet()) {
			System.out.println(chave + ": " + mcCardapio.get(chave));
		}
	}
}
